const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')

const service = require('feathers-knex')
const knex = require('knex')

const db = knex({
  client: 'sqlite3',
  connection: {
    filename: './dev.sqlite3'
  }
})

// Create a feathers instance.
const app = express(feathers())
// Turn on JSON parser for REST services
app.use(express.json())
// Turn on URL-encoded parser for REST services
app.use(express.urlencoded({ extended: true }))
// Enable REST services
app.configure(express.rest());

app.use('/api/users', service({
  Model: db,
  name: 'User'
}))

app.use('/api/pictures', service({
  Model: db,
  name: 'Picture'
}))

app.use(express.errorHandler())

// Start the server.
const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})
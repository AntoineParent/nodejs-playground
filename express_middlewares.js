#! /usr/bin/env node
/*
const express = require('express')
const app = express()

app.get('/', nasaFunction)
app.post('/create-user', createUser)

//pipeline d'execution, la fonction exécutée est middleware1, ...dans l'ordre
app.use(middleware1)
app.use(middleware2)
app.use(middleware3)
app.use(errorHandler)
app.listen(3000)

//c'est quoi un mmiddleware
app.use(doNothing)
let doNothing = function (request, response, next) {
    next() // ne fait rien, passe à la site
}

app.use(logger)
let logger = function (request, response, next) {
    console.log('method', request.method)
    console.log('url', request.url)
    next() // ne fait rien, passe à la site
}*/
const express = require('express')
const axios = require('axios')

const port = process.env.PORT || 3000
const app = express()

//test middleware (voir commentaires)
let logger = function (request, response, next) {
    console.log('method', request.method)
    console.log('url', request.url)
    next() // on apelle ça continuation
}
app.use(logger)

//
const createHTML = names => {
    let html = "<ul>" + names.map(person => person.name).reduce((accu, name) => accu  + "<li>" + name + "</li>", "") + "</ul>"
    return html
}
app.get('/api/astros', async function (request, response) {
    let axiosResp = await axios.get('http://api.open-notify.org/astros.json')
    response.send(createHTML(axiosResp.data.people))
})
app.listen(port, function () {
    console.log("Server listening on port 3000")
})
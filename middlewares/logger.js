const fsp = require('fs').promises
const os = require('os')

module.exports = function(filepath) {

    return function(req, res, next) {
        let message = `${req.requestTime} - ${req.method} ${req.url} ${os.EOL}`
        fsp.appendFile(filepath, message)
        next()
    }
}
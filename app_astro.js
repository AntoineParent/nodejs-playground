#! /usr/bin/env node

const requestTimeMiddleware = require('./middlewares/requestTime')
const loggerMiddleware = require('./middlewares/logger')
const errorhandler = require('errorhandler')
const logFilePath = require('./config').LOGFILEPATH

const express = require('express')
const axios = require('axios')

const port = process.env.PORT || 3000
const app = express()

app.use(requestTimeMiddleware)
app.use(loggerMiddleware(logFilePath))

const createHTML = names => {
    let html = "<ul>" + names.map(person => person.name).reduce((accu, name) => accu  + "<li>" + name  + "</li>", "") + "</ul>"
    return html
}

app.get('/api/astros', async function (request, response) {
    let axiosResp = await axios.get('http://api.open-notify.org/astros.json')
    response.send(createHTML(axiosResp.data.people))
})
app.use(errorhandler)

app.listen(port, function () {
    console.log(`Server listening on port ${port}`)
})
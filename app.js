#! /usr/bin/env node
/*EXPRESS*/
HTTP
const http = require('http');
const server = http.createServer()
const port = process.env.port || 8080

//create a server object:
server.on('request', function(request, response) {
    console.log ('METHOD:', request.method)
    console.log ('URL:', request.url)
    console.log ('HEADERS:', request.headers)

    let data = ''
    request.on('data', function (chunk) {
        console.log('BODY CHUNK...', chunk.length, chunk)
        response.write('-' + chunk.toString() + '\n')
        //data += '-' + chunk.toString()
    })

    request.on('end', function() {
        response.write(200)
        response.write('hello!')
    })
})

server.listen(port)
console.log("Listening on port", port)